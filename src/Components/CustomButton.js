import React from "react";
import { View, Text ,TextInput, StyleSheet, Pressable } from "react-native";

const CustomButton = ({ onPress, text, type = "Primary", bgColor, fgColor}) => {
    return(
        <Pressable 
        onPress={onPress}
        style={[styles.container,
        styles[`container_${type}`],
        bgColor ? {backgroundColor: bgColor} : {},
        
        ]}>
            <Text style={[
                styles.text, 
                styles[`text_${type}`],
                fgColor ? {color: fgColor} : {},
        ]}
        >{text}</Text>
        </Pressable>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        padding: 15,
        marginVertical: 10,

        alignItems: 'center',
        borderRadius: 5,
    },
    container_Primary:{
        backgroundColor: '#3B71F3',

    },
    container_Secondary:{

    },
    container_Tertiary:{

    },
    text: {
        fontWeight: 'bold',
        color: 'white',
    },
    text_Primary:{

    },
    text_Secondary:{

    },
    text_Tertiary:{
        color: 'grey',

    },
})


export default CustomButton;