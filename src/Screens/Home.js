import { View, Text, TouchableOpacity, Button} from 'react-native'
import React from 'react'
import { useNavigation } from "@react-navigation/native";

const Home = () => {
    const navigation = useNavigation();
    const Navigate = () => {
        navigation.navigate('SignIn')
    } 

  return (
    <View>
    <Button title="Signin" onPress={Navigate}></Button>
    </View>
  )
}

export default Home