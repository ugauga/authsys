import React, {useState}from "react";
import { View, Text, Image, StyleSheet, useWindowDimensions, ScrollView } from "react-native";
import Logo from '../../assets/imgs/Logo_1.png'
import CustomInput from '../Components/CustomInput'
import CustomButton from "../Components/CustomButton";
import SocialSigninButtons from "../Components/SocialSigninButtons";
import { useNavigation } from "@react-navigation/native";
import { useForm, Controller } from "react-hook-form";
const SignIn = () => {
    const {control, handleSubmit, formState: {errors}} = useForm();
    console.log(errors)

    const {height} = useWindowDimensions();
    const navigation = useNavigation();

    const onSignInPressed = (data) => {
        console.warn("sign in");
        //validate user

        navigation.navigate('Home');
    }
    const onForgotPasswordPressed = () => {
        console.warn("forgot password");
    }
    const onSignUpPressed = () => {
        console.warn("Create account");
        navigation.navigate('SignUp');
    }

    return(
        <ScrollView>
        <View style={styles.container}>
            <Image 
            source={Logo} 
            style={[styles.logo, {height: height * 0.3}]} 
            resizeMode="contain"/>

            <CustomInput name="username" placeholder="Username" control={control} rules={{required: true}}/>
            <CustomInput name="password" placeholder="Password" control={control} rules={{required: true}} secureTextEntry={true}/> 
            
            <CustomButton text="Sign In" onPress={handleSubmit(onSignInPressed)}/>
            <CustomButton 
            text="Forgot Password?" 
            onPress={onForgotPasswordPressed}
            type="Tertiary"
            />
            <SocialSigninButtons />

            <CustomButton 
            text="Don't have an Account? Create one" 
            onPress={onSignUpPressed}
            type="Tertiary"
            />
        </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'center',
        padding: 20,
    },
    logo: {
        width: '70%',
        maxWidth: 300,
        maxHeight: 200,
    }
})

export default SignIn;