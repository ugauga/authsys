import React, {useState}from "react";
import { View, Text, StyleSheet, useWindowDimensions, ScrollView } from "react-native";
import CustomInput from '../Components/CustomInput'
import CustomButton from "../Components/CustomButton";
import SocialSigninButtons from "../Components/SocialSigninButtons";
import { useNavigation } from "@react-navigation/native";

const SignUp = () => {

    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [passwordRepeat, setPasswordRepeat] = useState('');

    const {height} = useWindowDimensions();
    const navigation = useNavigation();

    const onRegisterPressed = () => {
        console.warn("Register");
    }

    const onSignInPressed = () => {
        console.warn("Redirect");

        navigation.navigate('SignIn')
    }

    return(
        <ScrollView>
        <View style={styles.container}>
            <Text style={styles.title}>Create an account</Text>

            <CustomInput placeholder="Username" value={username} setValue={setUsername} secureTextEntry={false}/>
            <CustomInput placeholder="Email" value={email} setValue={setEmail} secureTextEntry={false}/>
            <CustomInput placeholder="Password" value={password} setValue={setPassword} secureTextEntry={true}/> 
            <CustomInput placeholder="Repeat Password" value={passwordRepeat} setValue={setPasswordRepeat} secureTextEntry={true}/> 
            
            <CustomButton text="Register" onPress={onRegisterPressed}/>


            <SocialSigninButtons />
            
            <CustomButton 
            text="Have an account? Sign In" 
            onPress={onSignInPressed}
            type="Tertiary"
            />
        </View>
        </ScrollView>
    )
};

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'center',
        padding: 20,
    },
    logo: {
        width: '70%',
        maxWidth: 300,
        maxHeight: 200,
    },
    title:{
        fontSize:24,
        fontWeight: 'bold',
        color: '#051C60',
        margin: 10,
    }
})






export default SignUp;